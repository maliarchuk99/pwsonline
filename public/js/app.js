let app = angular.module('pwsonline', [])

app.controller('Ctrl', [ '$http', function($http){
    console.log("Controller ")
    let ctrl = this

    ctrl.newData = {
        firstName: '',
        secondName: '',
        year: 1999
    }

    $http.get('/person').then(
        function(res) {
            ctrl.data = res.data
        },
        function(err) {}
    )

    ctrl.send = function() {
       $http.put('/person', ctrl.newData).then(
           function(res){
               ctrl.data = res.data
           },
           function(err){}
       )
    }

    ctrl.change = function() {
        $http.put('/person', ctrl.data).then(
            function(res){
                ctrl.data = res.data
            },
            function(err){}
        )
     }
   
}])